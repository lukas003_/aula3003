const express = require('express');
const app = express();
const port = 8000;
//enviar informação via body
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//habilitar servidor
app.listen(port,() => {
    console.log(`Projeto executando na porta ${port}`);
});

//chamada simples retornando texto
app.get('/',(req,res) => {
    res.send('{message: aluno encontrado}');
});

//chamada simples com JSON
app.get('/aluno',(req,res) => {
    res.json({ sucesso: res.statusCode == 200,
               message: 'aluno encontrado',
               nome: 'Lukas',
               sobrenome: 'Oliveira'
            });
});

app.get('/aluno',(req,res) => {
    res.json({ sucesso: res.statusCode == 200,
               message: 'aluno encontrado',
               nome: 'Lukas',
               sobrenome: 'Oliveira'
            });
});

//chamada com filtros
app.get('/aluno/filtro',(req,res) => {
    let obj = req.query;
    res.json({ sucesso: res.statusCode == 200,
               message: 'aluno encontrado',
               nome: obj.nome,
               sobrenome: obj.sobrenome,
               idade: parseInt(obj.idade)
            });
});

//Post dos dados
app.post('/aluno',(req, res) => {
    let dados = req.body;
    let ret = "Dados enviados, "
    ret += `Nome: ${dados.nome}, `;
    ret += `Sobrenome: ${dados.sobrenome}, `;
    ret += `Idade: ${dados.idade}`;
    res.send("{message: " + ret + "}");
});